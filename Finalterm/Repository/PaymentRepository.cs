﻿using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class PaymentRepository:Repository<Payment>
    {
        public PaymentRepository(FinalProjectEntities fr):base(fr)
        {
        }
    }
}