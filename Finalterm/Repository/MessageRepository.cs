﻿using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class MessageRepository:Repository<Message>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public MessageRepository(FinalProjectEntities fr):base(fr)
        {

        }
        public void update(Message o)
        {
            Message m = f.Messages.Find(o.Id);
            m.Id = o.Id;
            m.Messages = o.Messages;
            m.Sender_Id = o.Sender_Id;
            m.Status = o.Status;
            f.SaveChanges();
        }
    }
}