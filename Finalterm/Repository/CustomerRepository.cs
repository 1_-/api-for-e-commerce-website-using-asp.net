﻿using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class CustomerRepository:Repository<Customer>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public CustomerRepository( FinalProjectEntities fr):base(fr)
        {
        }
        public Customer GetById(string id)
        {
            return f.Set<Customer>().Find(id);
        }
        public void Update(Customer customer)
        {
            f.Entry(customer).State = EntityState.Modified;
            f.SaveChanges();
        }
        public void update(Customer c,int s)
        {
            Customer c1= f.Customers.Find(c.Id);
            Login l1 = f.Logins.Find(c.Id);
            c1.Id = c.Id;
            c1.Name = c.Name;
            c1.PhoneNumber = c.PhoneNumber;
            c1.Email = c.Email;
            c1.Address = c.Address;
            l1.Status = s;
            c1.Points = c1.Points;
            f.SaveChanges();
        }
    }
}