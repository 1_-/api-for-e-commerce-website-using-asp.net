﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Models
{
    public class AdminDashboard
    {
        public int NoOfOrders { get; set; }
        public int NoOfSales { get; set; }
        public int NoOfCustomer { get; set; }
        public List<Order> orders { get; set; }
        public int Jan { get; set; }
        public int feb { get; set; }
        public int march { get; set; }
        public int april { get; set; }
        public int may { get; set; }
        public int jun { get; set; }
        public int july { get; set; }
        public int Aug { get; set; }
        public int Sept { get; set; }
        public int Nov { get; set; }
        public int Dec { get; set; }
        public int Oct { get; set; }



        public int Online { get; set; }

    }
}