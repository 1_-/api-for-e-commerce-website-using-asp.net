﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finalterm.Validation
{
    public class DiscountValid: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            IRepository<Discount> drepo = new DiscountRepository(new FinalProjectEntities());

            string s = value.ToString();

            Discount d = drepo.GetAll().Where(p => p.Id == s).FirstOrDefault();
            if(d==null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}