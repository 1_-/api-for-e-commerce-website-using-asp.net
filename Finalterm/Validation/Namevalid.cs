﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finalterm.Validation
{
    public class Namevalid:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
           
            if(value==null)
            {
                return false;
            }
            string s = value.ToString().Substring(0,1);
            try
            {
                int i = int.Parse(s);
                return false;

            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}