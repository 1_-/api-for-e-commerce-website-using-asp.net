﻿using Finalterm.Interface;
using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finalterm.Validation
{
    public class CheckUnitId:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            FinalProjectEntities fr = new FinalProjectEntities();
            int s =(int)value;
            Unit ut = fr.Units.Where(p=>p.Id==s).FirstOrDefault();
            if(ut==null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}