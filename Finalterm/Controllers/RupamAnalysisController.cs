﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;
//using System.Text.Json;

namespace Finalterm.Controllers
{
    public class RupamAnalysisController : SecureController
    {
        //IRepository<Product> prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Catagory> crrepo = new CatagoryRepository(new FinalProjectEntities());
        IRepository<Order> orrepo = new OrderRepository(new FinalProjectEntities());
        // GET: RupamAnalysis
        public ActionResult Analysis()
        {
            AnalysisClass ad = new AnalysisClass();
            List<Order> ol = orrepo.GetAll().ToList();
            List<Order> fl = new List<Order>();
            
            ad.Jan = 0;
            ad.feb = 0;
            ad.march = 0;
            ad.april = 0;
            ad.may = 0;
            ad.jun = 0;
            ad.july = 0;
            ad.Aug = 0;
            ad.Sept = 0;
            ad.Oct = 0;
            ad.Nov = 0;
            ad.Dec = 0;
            string s = DateTime.Now.ToString();

            foreach (var item in ol)
            {
                if (item.time.ToString().Substring(0, 2) == "1/")
                {
                    ad.Jan += item.Product.Sell_Price-item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "2/")
                {
                    ad.feb += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "3/")
                {
                    ad.march += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "4/")
                {
                    ad.april += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "5/")
                {
                    ad.may += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "6/")
                {
                    ad.jun += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "7/")
                {
                    ad.july += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "8/")
                {
                    ad.Aug += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "9/")
                {
                    ad.Sept += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "10")
                {
                    ad.Oct += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "11")
                {
                    ad.Nov += item.Product.Sell_Price - item.Product.Buy_Price;
                }
                else if (item.time.ToString().Substring(0, 2) == "12")
                {
                    ad.Dec += item.Product.Sell_Price - item.Product.Buy_Price;
                }
            }
            List<Order> ot = orrepo.GetAll().ToList();
            List<Catagory> lc = crrepo.GetAll().ToList();
            List<Profitcatagory> lp = new List<Profitcatagory>();
            foreach (var item in lc)
            {
                Profitcatagory pl = new Profitcatagory();
                pl.Name = item.Name;
                pl.profit = orrepo.GetAll().Where(p => p.Product.Catagory_Id == item.Id).Count();
                lp.Add(pl);
            }
            var a = ot.OrderBy(p => p.Payment.Amount).ToList();
            
            ViewBag.a = a;
            ViewBag.data = lp;
            return View(ad);
        }
       
    }
}