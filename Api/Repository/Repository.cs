﻿using Finalterm.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Models;

namespace Finalterm.Repository
{
    public class Repository<TEntity>:IRepository<TEntity> where TEntity:class
    {
        FinalProjectEntities fr = new FinalProjectEntities();
        public Repository(FinalProjectEntities fr)
        {
            this.fr = fr;
        }
        public TEntity GetById(string id)
        {
            try
            {
                int i = int.Parse(id);
                return fr.Set<TEntity>().Find(i);
            }
            catch (Exception)
            {
                return fr.Set<TEntity>().Find(id);
            }
            
        }
        public IEnumerable<TEntity> GetAll()
        {
            return fr.Set<TEntity>().ToList();
        }
        public void Insert(TEntity entity)
        {
            fr.Set<TEntity>().Add(entity);
            fr.SaveChanges();
        }
        public void Delete(TEntity entity)
        {
            fr.Set<TEntity>().Remove(entity);
            fr.SaveChanges();
        }
    }
}