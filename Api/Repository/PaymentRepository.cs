﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class PaymentRepository:Repository<Payment>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public PaymentRepository(FinalProjectEntities fr) : base(fr)
        { }
            public void Update(Payment customer)
            {
                f.Entry(customer).State = EntityState.Modified;
                f.SaveChanges();
            }
        
    }
}