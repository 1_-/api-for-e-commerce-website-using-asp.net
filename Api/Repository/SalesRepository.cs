﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class SalesRepository : Repository<Sale>
    {
        public SalesRepository(FinalProjectEntities fr) : base(fr)
        {
        }
    }
}