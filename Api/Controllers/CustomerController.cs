﻿using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/customers")]
    public class CustomerController : ApiController
    {
        IRepository<Customer> crepo = new CustomerRepository(new FinalProjectEntities());
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        CustomerRepository cr = new CustomerRepository(new FinalProjectEntities());
        [Route("")]
        public IHttpActionResult Get()
        {
            var clist= crepo.GetAll().ToList();
            List<Customer> cl = new List<Customer>();

            foreach (var item in clist)
            {
                Customer c = new Customer();
                c.Id = item.Id;
                c.Image = item.Image;
                c.Name = item.Name;
                c.PhoneNumber = item.PhoneNumber;
                c.Points = item.Points;
                c.Address = item.Address;
                c.Email = item.Email;
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/"+c.Id, method = "Get", rel = "Specific Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/"+c.Id, method = "Put", rel = "Update Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/"+c.Id, method = "Delete", rel = "Delete Resource" });
                cl.Add(c);
            }
            return Ok(cl);
        }
        [Route("{id}",Name = "GetoneComm")]
        public IHttpActionResult Get(string id)
        {
            var clist = crepo.GetAll().ToList().Where(p=>p.Id==id).FirstOrDefault();

            if(clist==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Customer c = new Customer();
                c.Id = clist.Id;
                c.Image = clist.Image;
                c.Name = clist.Name;
                c.PhoneNumber = clist.PhoneNumber;
                c.Points = clist.Points;
                c.Address = clist.Address;
                c.Email = clist.Email;
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Get", rel = "All Resources" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Put", rel = "Update Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(c);
            }
        }
        [Route("")]
        public IHttpActionResult Post(Customer c)
        {
            if(c==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Random r = new Random();
                int ran = r.Next(999);
                Login l = new Login();
                string s= "C" + ran;
                l.ID = s;
                l.Password = r.Next(99999).ToString();
                l.Status = 0;
                l.Online = 0;
                l.Type = 0;
                lrepo.Insert(l);
                c.Id = s;
                crepo.Insert(c);
                return Created(Url.Link("GetoneComm", new { id =c.Id }), c);
            }
        }
        [Route("{id}")]
        public IHttpActionResult Put([FromUri]string id,[FromBody]Customer c)
        {
            var customer = crepo.GetById(id);
            
            if(customer==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                c.Id = customer.Id;
                cr.Update(c);
                return Created(Url.Link("GetoneComm", new { id = c.Id }), c);
            }
            
        }
        [Route("{id}")]
        public IHttpActionResult Delete(string id)
        {
            var c= crepo.GetById(id);
            crepo.Delete(c);
            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("{id}/logins")]
        public IHttpActionResult GetStatus(string id)
        {
            var lg= lrepo.GetById(id);
            if(lg==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Login l = new Login();
                l.ID = lg.ID;
                l.Status = lg.Status;
                return Ok(l);
            }
        }
    }
}
