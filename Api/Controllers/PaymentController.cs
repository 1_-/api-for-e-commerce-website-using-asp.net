﻿using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Payments")]
    public class PaymentController : ApiController
    {
        FinalProjectEntities
        context = new FinalProjectEntities();
        IRepository<Payment> papo = new PaymentRepository(new FinalProjectEntities());
        PaymentRepository cr = new PaymentRepository(new FinalProjectEntities());

        [Route("")]
        public IHttpActionResult Get()
        {
            var clist = papo.GetAll().ToList();
            List<Payment> cl = new List<Payment>();

            foreach (var item in clist)
            {
                Payment c = new Payment();
                c.Id = item.Id;
                c.Method = item.Method;
                c.Amount = item.Amount;
                c.Status = item.Status;

                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/", method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Get", rel = "Specific Resource" });

                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Delete", rel = "Delete Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Put", rel = "Update Resource" });
                cl.Add(c);
            }
            return Ok(cl);

        }
        [Route("{id}", Name = "GetProByd")]
        public IHttpActionResult Get(string id)
        {


            var clist = papo.GetAll().ToList().Where(p => p.Id.ToString() == id).FirstOrDefault();

            if (clist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Payment c = new Payment();
                c.Id = clist.Id;
                c.Amount = clist.Amount;
                c.Method = clist.Method;
                c.Status = clist.Status;



                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments", method = "Get", rel = "All Resources" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Delete", rel = "Delete Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Payments/" + c.Id, method = "Put", rel = "Update Resource" });
                return Ok(c);
            }

        }


        [Route("{id}")]
        public IHttpActionResult Delete([FromUri]int id)
        {
            var c = papo.GetById(id.ToString());
            papo.Delete(c);
            return StatusCode(HttpStatusCode.NoContent);




        }
        [Route("Customers")]
        public IHttpActionResult Post(Payment product)
        {
            //Random rf = new Random();
            //Login c = new Login();
            Payment c = new Payment();

            c.Id = product.Id;
            c.Status = product.Status;
            c.Amount = product.Amount;
            c.Method = product.Method;

            papo.Insert(product);
            return Created(Url.Link("GetoneComm", new { id = product.Id }), product);
        }
        [Route("{id}")]
        public IHttpActionResult Put([FromUri]string id, [FromBody]Payment c)
        {
            var customer = papo.GetById(id);

            if (customer == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                c.Id = customer.Id;
                cr.Update(c);
                return Created(Url.Link("GetoneComm", new { id = c.Id }), c);
            }

        }







    }
}

