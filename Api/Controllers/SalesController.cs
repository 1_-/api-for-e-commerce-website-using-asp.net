﻿using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Sales")]
    public class SalesController : ApiController
    {
        IRepository<Sale> papo = new SalesRepository(new FinalProjectEntities());
        SalesRepository cr = new SalesRepository(new FinalProjectEntities());

        [Route("")]
        public IHttpActionResult Get()
        {
            var clist = papo.GetAll().ToList();
            List<Sale> cl = new List<Sale>();

            foreach (var item in clist)
            {
                Sale c = new Sale();
                c.Id = item.Id;
                c.profit = item.profit;
                c.O_Id = item.O_Id;

                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales", method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales/" + c.Id, method = "Get", rel = "Specific Resource" });

                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales/" + c.Id, method = "Delete", rel = "Delete Resource" });
                cl.Add(c);
            }
            return Ok(cl);

        }
        [Route("{id}", Name = "GetProductById")]
        public IHttpActionResult Get(int id)
        {

            var clist = papo.GetAll().ToList().Where(p => p.Id == id).FirstOrDefault();

            if (clist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Sale c = new Sale();
                c.Id = clist.Id;
                c.profit = clist.profit;
                c.O_Id = clist.O_Id;


                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales", method = "Get", rel = "All Resources" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales/" + c.Id, method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/sales/" + c.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(c);
            }

        }


        [Route("{id}")]
        public IHttpActionResult Delete([FromUri]int id)
        {
            var c = papo.GetById(id.ToString());
            papo.Delete(c);
            return StatusCode(HttpStatusCode.NoContent);




        }




    }
}







    

